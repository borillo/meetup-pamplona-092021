![1, 2, 3 ... Refactoring!!](images/portada.png)

## ✅ Contexto

- 💡 Refactoring con equipos: MOB programming.
- 💡 Principios de backend en frontend.
- 🎥 [Programación funcional: Próximamente en un lenguaje de programación cerca de usted](https://www.youtube.com/watch?v=y0GwxCDTJvA)
- 🧠 Estudiando programación funcional.
- 💡 TypeScript + [fp-ts](https://github.com/gcanti/fp-ts).

## ✅ Estructura del proyecto

- 🧠 Domain-Driven Design:
  - **Strategic Design**: Ubiquitous Language, Context Mapping, Event Storming, etc.
  - **Tactical Design**: OOP + Aggregates.
- 💡 ![Ejemplo dominio funcional](images/functional-domain-example.png)
- 🎥 [Functional architecture - The pits of success](https://www.youtube.com/watch?v=US8QG9I1XW0)
- 📖 Domain modeling made functional (Scott Wlaschin).
- 💡 Estructura técnica vs estructura funcional:
  - Un primer nivel por cada dominio.
  - Misma abstracción en varios dominios.

## ✅ Foco en los “value objects”

- 💩 Código duplicado + Falta de niveles de abstracción.
- 🧠 Tell, don't ask. Ley de Demeter.
- 💡 Previo al refactoring de diseño. Primero semántica y naming.
- 💡 Foco atractor de código.
- 💩 utils & helpers.
- 🚀 Encapsulación de colecciones (💩 Código duplicado + Acoplamiento).

## ✅ Manejo de la opcionalidad

- 💡 Valores inexistentes.
- 💩 Programación defensiva (`Optional chaining` or `Nullish coalescing operator`).
- 💡 NullObject pattern.
- 🧠 Functional programming.

## ✅ Manejo de excepciones

- 💩 Programación defensiva.
- 💩 Enfoque tradicional:
  - Paso de mensajes.
  - Ruptura del flujo + exception handler global.
  - Falta de contexto.
  - No accionables. Excepciones ignoradas/silenciadas.
- 💡 Lo convertimos en un `value`.
- 🧠 Functional programming.

## ✅ Decoración

- 💩 Separación de responsabilidades: audit, logging, caching.
- 💡 Permiten componerse.
- 💡 Métodos factoría para generar las decoraciones.
- 💡 Configurable en test para evitar por ejemplo el caching.

## ✅ Recursos

- 🚀 [io-ts](https://github.com/gcanti/io-ts): Runtime type system for IO decoding/encoding.
- 🚀 [monocle-ts](https://github.com/gcanti/monocle-ts): Functional optics: a (partial) porting of Scala monocle.

# ❔ Q&A

> Ricardo Borillo

> borillo@gmail.com

> https://twitter.com/borillo
